<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

require "models/posts.php";
require "models/users.php";

Route::get('/', function()
{
    $posts = getPosts();
	return View::make('social.home')->withPosts($posts);
});

// Perform search and display results
Route::get('friends', function()
{
    $users = getUsers();
  return View::make('social.friends')->withUsers($users);
});