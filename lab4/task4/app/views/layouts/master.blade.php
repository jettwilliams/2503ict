<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Social Network</title>
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
        
        <style>
          .blog-post {
            padding: 30px;
          }
        </style>
        
    </head> 
    <body>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href=".">Social Network</a>
            </div>
            
            <div>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Photos</a></li>
                <li><a href="friends">Friends</a></li>
                <li><a href="#">Login</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        
        <div class="container">
          
          <div class="row">
            
            <div class="col-sm-8 blog-main">

                @yield('content')
    
              <nav>
                <ul class="pager">
                  <li><a href="#">Previous</a></li>
                  <li><a href="#">Next</a></li>
                </ul>
              </nav>
    
            </div><!-- /.blog-main -->
    
            <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
              <p>Name:<br /><input type="text" /></p>
              <p>Message:<br /><textarea rows="8" cols="20"></textarea></p>
              <p><input type="submit" value="Send" /></p>
            </div><!-- /.blog-sidebar -->
    
          </div><!-- /.row -->
    
        </div><!-- /.container -->
        
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        
    </body>
</html>