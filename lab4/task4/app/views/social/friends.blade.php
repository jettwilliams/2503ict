@extends('layouts.master')

@section('content')
    @foreach($users as $user)
      <div class="blog-post">
              <img src="{{{ $user['image'] }}}" width="150" height="100" alt="image" />
              <h4 class="blog-post-meta"><a href="#">{{{ $user['name'] }}}</a> joined {{{ $user['date'] }}}</h4>
            </div><!-- /.blog-post -->
    @endforeach
@stop