@extends('layouts.master')

@section('content')
    @foreach($posts as $post)
      <div class="blog-post">
              <img src="{{{ $post['image'] }}}" width="150" height="100" alt="image" />
              <h4 class="blog-post-meta">{{{ $post['date'] }}} by <a href="#">{{{ $post['name'] }}}</a></h4>
              <p>{{{ $post['message'] }}}</p>
            </div><!-- /.blog-post -->
    @endforeach
@stop